using Classes;

namespace Storages;

public abstract class Vehicle
{
    private static int _nextId = 1;
        
    public int Id { get; private set; }
    public string Brand { get; set; }
    public float Weight { get; set; }
    private List<WheelAxis> Axles { get; set; }

    protected abstract List<WheelAxis> CreateAxis();

    public Vehicle(string brand, float weight)
    {
        Id = _nextId++;
        Brand = brand;
        Weight = weight;
        Axles = CreateAxis();
    }

    protected Vehicle()
    {
        throw new NotImplementedException();
    }

    public bool CheckWheelSuitability(Wheel wheel)
    {
        var wheelCnt = 0;
        Axles.ForEach(a => wheelCnt += a.WheelList.Count);
        var vehicleMaxLoad = Weight / wheelCnt;
        return vehicleMaxLoad <= wheel.MaxLoad;
    }

}