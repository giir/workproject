namespace Storages;

public class Wheel
{
    private static int _nextId = 1;
    public int Id { get; private set; }
    public int Radius { get; private set; }
    public float MaxLoad { get; private set; }
    public Wheel(int radius, float maxLoad)
    {
        Id = _nextId++;
        Radius = radius;
        MaxLoad = maxLoad;
    }
    public string GetDetails()
    {
        return $"Wheel ID: {Id}, Radius: {Radius}, Max Load: {MaxLoad}";
    }
}