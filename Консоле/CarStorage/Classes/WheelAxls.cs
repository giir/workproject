using Storages;

namespace Classes
{

    public class WheelAxis
    {
        public WheelAxis(List<Wheel> wheelList)
        {
            WheelList = wheelList;
        }

        public List<Wheel> WheelList { get;  set; }
    }
}