using Storages;

namespace Classes
{
    public class Car : Vehicle
    {
        public Car(string brand, float weight) : base(brand, weight)
        {
        }
        
        protected override List<WheelAxis> CreateAxis()
        {
            var radius = 16;
            var maxLoad = 400;
            
            return new List<WheelAxis>()
            {
                new WheelAxis(new List<Wheel>()
                {
                    new Wheel(radius, maxLoad),
                    new Wheel(radius, maxLoad)
                }),
                new WheelAxis(new List<Wheel>()
                {
                    new Wheel(radius, maxLoad),
                    new Wheel(radius, maxLoad)
                })
            };
        }
    }
}