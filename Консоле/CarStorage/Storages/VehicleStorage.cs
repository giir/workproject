using Classes;

namespace Storages
{
    public class VehicleStorage
    {
        private readonly List<Vehicle> _vehicles = new List<Vehicle>();
        
        public List<Vehicle> GetVehicles()
        {
            return _vehicles;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }
        public Vehicle? GetVehicleById(int id)
        {
            return _vehicles.Find(v => v.Id == id);
        }
    }
}