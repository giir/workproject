namespace Storages
{
    public class WheelStorage
    {
        private readonly List<Wheel> _wheels = new List<Wheel>();
        public List<Wheel> GetWheels()
        {
            return _wheels;
        }
        public void AddWheel(int radius, float maxLoad)
        {
            var wheel = new Wheel(radius, maxLoad);
            _wheels.Add(wheel);
        }
        public Wheel? GetWheelById(int id)
        {
            return _wheels.Find(w => w.Id == id);
        }
    }
}