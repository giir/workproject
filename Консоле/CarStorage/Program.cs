﻿using Classes;
using Storages;



namespace AutobaseApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var autobase = new VehicleStorage();

            autobase.AddVehicle(new Car("Легковой автомобиль", 1800));
            autobase.AddVehicle(new Truck("Трактор", 5000));
            autobase.AddVehicle(new Bus("Автобус", 8000));
            autobase.AddVehicle(new Motorcycle("Мотоцикл", 150));
            
            WheelStorage wheelStorage = new WheelStorage();
            
            //TODO: явную декларацию "Autobase" уже никто не использует, пиши просто var  +
            //TODO: всю логику в try-catch +
            var operation = 0;
            do
            {
                try
                {
                    Console.WriteLine("Выберите операцию:");
                    Console.WriteLine("1. Транспортное средство");
                    Console.WriteLine("2. Добавить Транспортное средство");
                    Console.WriteLine("3. Колеса");
                    Console.WriteLine("4. Добавить колеса");
                    Console.WriteLine("5. Проверьте пригодность колеса");
                    Console.WriteLine("6. Exit");
                    Console.Write("Введите номер операции: ");
                    operation = int.Parse(Console.ReadLine());
                    switch (operation)
                    {
                        case 1:
                            ListVehicles(autobase);
                            break;
                        case 2:
                            AddVehicle(autobase);
                            break;
                        case 3:
                            ListWheels(wheelStorage);
                            break;
                        case 4:
                            AddWheel(wheelStorage);
                            break;
                        case 5:
                            CheckWheelSuitability(autobase, wheelStorage);
                            break;
                        case 6:
                            Console.WriteLine("Выход...");
                            break;
                        default:
                            Console.WriteLine("Недопустимый номер операции.");
                            break;
                    }
                    Console.WriteLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Произошла ошибка: " + e.Message);
                }
            } while (operation != 6);

            static void ListVehicles(VehicleStorage autobase)
            {
                Console.WriteLine("Транспортные средства в автобазе:");
                foreach (Vehicle vehicle in autobase.GetVehicles())
                {
                    Console.WriteLine($"ID: {vehicle.Id}, Бренд: {vehicle.Brand}, Вес: {vehicle.Weight}");
                }
            }

            static void AddVehicle(VehicleStorage autobase)
            {
                Console.Write("Введите бренд: ");
                var brand = Console.ReadLine();
                
                Console.Write("Введите вес: ");
                var weight = float.Parse(Console.ReadLine());
                
                Console.Write("Введите ось: ");
                var axles = int.Parse(Console.ReadLine());
                
                Console.Write("Введите тип ТС: ");
                var typeTs = Console.ReadLine();

                autobase.AddVehicle(typeTs switch
                {
                    "авто" => new Car(brand, weight),
                    "мото" => new Motorcycle(brand, weight),
                    "автобус" => new Bus(brand, weight),
                    "грузовик" => new Truck(brand, weight),
                    _ => throw new ArgumentException("Не верный тип ТС")
                });

                Console.WriteLine("Транспортное средство успешно добавлено.");
            }

            static void ListWheels(WheelStorage wheelStorage)
            {
                Console.WriteLine("Колеса в хранилище для колес:");
                foreach (var wheel in wheelStorage.GetWheels())
                {
                    Console.WriteLine($"ID: {wheel.Id}, Колесо: {wheel.Radius}, Максимальная нагрузка: {wheel.MaxLoad}");
                }
            }

            static void AddWheel(WheelStorage wheelStorage)
            {
                Console.Write("Введите количество осей: ");
                int radius = int.Parse(Console.ReadLine());
                Console.Write("Введите максимальную нагрузку: ");
                float maxLoad = float.Parse(Console.ReadLine());
                wheelStorage.AddWheel(radius, maxLoad);
                Console.WriteLine("Колесо добавлено успешно.");
            }

            static void CheckWheelSuitability(VehicleStorage autobase, WheelStorage wheelStorage)
            {
                Console.Write("Введите ID транспортного средства: ");
                int vehicleId = int.Parse(Console.ReadLine());
                Console.Write("Введите ID колеса: ");
                int wheelId = int.Parse(Console.ReadLine());
                Vehicle vehicle = autobase.GetVehicleById(vehicleId);
                if (vehicle != null)
                {
                    Wheel wheel = wheelStorage.GetWheelById(wheelId);
                    if (wheel != null)
                    {
                        bool isSuitable = vehicle.CheckWheelSuitability(wheel);
                        Console.WriteLine($"Колесо {(isSuitable ? "подходит" : "не подходит")}  для данного транспортного средства.");
                    }
                    else
                    {
                        Console.WriteLine("Колесо не найдено.");
                    }
                }
                else
                {
                    Console.WriteLine("Транспортное средство не найдено.");
                }

            }
           

        }
    }
    //TODO: каждый класс вынести отдельный модуль(файл *.cs). Назвать так же как файл, поместить в отдельную папку, соблюсти пространства имен
    //+



    //TODO: допустимо. Но 




    //TODO: Axles - это кол-во осей? а если это камаз у которого на первой оси 2 колеса, на 2 и 3ей оси - по 4 колеса?
    //TODO: totalMaxLoad же для него должно посчитаться = wheel.MaxLoad * 10 (колес), а не wheel.MaxLoad * 3; +
    //TODO: может не удачный пример, но я хотел, чтобы ТС ты реализовал через наследование. Чтобы был базовый класс для ТС,
    //TODO: от него унаследованы автомобили (от них легковые и грузовые), автобусы, мотоциклы. Есть еще класс осей,
    //TODO: каждый класс ТС уже заранее должен знать сколько в нем осей и колес. Потом на эту модель еще будем докручивать разное. 



}